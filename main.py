import csv

class Child:
    name = ''
    arm = True # правша
    vision = {} # ряд,  [min: парта 1, max: парта 2]
    hight = ''
    problem = False
    
    def __init__(self, name):
        self.name = name
        self.problem_with = {}
        self.can_sit = []

    def set_arm(self, arm):
        self.arm = arm

    def set_vision(self, vision):
        self.vision = vision

    def set_hight(self, hight):
        self.hight = hight

    def set_problem(self, problem):
        self.problem = problem

    def set_problem_with_append(self, problem_with):
        self.problem_with.append(problem_with)

    def print(self):
        print(self.name, self.arm, self.vision, self.hight, self.problem, self.problem_with, self.can_sit)


def create_child():
    child_list = []
    with open('Учащиеся.csv', encoding="utf8") as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            child = Child(row['Имя'])
            child.set_arm(row['Рабочая рука'] == 'Правая')
            vision = {}
            if row['Ограничение по зрению'] == '':
                vision = {0: [0, 0]}
            else:
                min = int(row['Ограничение по зрению'][0])
                if row['Ограничение по зрению'][2] == 'п':
                    max = min
                else:
                    max = int(row['Ограничение по зрению'][2])
                if 'средний' in row['Ограничение по зрению']:
                    vision = {2: [min, max]}
                else:
                    vision = {0: [min, max]}
            child.set_vision(vision)
            child.set_hight(row['Рост'])
            child.set_problem(row['Проблема с концентрацией внимания'] == 'TRUE')
            child_list.append(child)
    return child_list

def problem_with_child(child_list):
    with open('Совместимость с другим учащимся.csv', encoding="utf8") as csvfile:
        reader = csv.reader(csvfile)
        index = 0
        index_second = 0
        name = []
        
        for row in reader:
            if index == 0:
                name = row
                index+=1
                continue
            index_second = -1
            for value in row:
                index_second+=1
                if index_second == 0 or value == '':
                    continue
                for child in child_list:
                    if child.name == name[index]:
                        child.problem_with[name[index_second]] = value
                        continue
                    if child.name == name[index_second]:
                        child.problem_with[name[index]] = value
                        continue
            index+=1

def sort_children_can_sit(student_list): # допустимые места
    for child in student_list:
        for desk in range(32):
            flag = True
            if child.arm == False and desk%2 == 1 and desk<18:
                flag = False
            if child.vision.get(2) != None and int(desk/2)!=1:
                flag = False
            if child.vision.get(0)!= None and child.vision[0][0] !=0:
                if child.vision[0][0]<int(desk/6)+1>child.vision[0][1]:
                    flag = False
            if flag:
                child.can_sit.append(desk)
        
def sort_1(student):
    return len(student.can_sit)

def sit(student_list):
    class_list = {}
    index = 0
    student_list = sorted(student_list, key=sort_1)
    for child in student_list:
        sited = False
        save = 0
        for place in child.can_sit:
            save = place
            if class_list.get(place+1) == None:
                if class_list.get(place) not in child.problem_with.keys() and class_list.get(place+2) not in child.problem_with.keys():
                    class_list[place+1] = child.name
                    sited = True
                    break
        if not sited:
            # old = class_list[2]
            # class_list[2] = child.name
            # class_list[save] = old
            # # Эгеге этого ни кто не должен увуидеть
            # # print("Ну пиздос "+ child.name +"не смог посадить попу и ушёл какать")
            try_replace(class_list, student_list, child)
            pass

    return class_list

def try_replace(class_list, st_list, stud2):
    for k, v in class_list.items():
        flag = False
        for stud1 in st_list:
            if v in stud2.can_sit and class_list.get(k-1) not in stud2.problem_with.keys() and class_list.get(k+1) not in stud2.problem_with.keys():
                    flag = True
            new_index = max(class_list.keys())
            if flag and new_index in stud1.can_sit and class_list.get(new_index-1) not in stud1.problem_with and class_list.get(new_index+1) not in stud1.problem_with:
                class_list[v] = stud2.name
                class_list[new_index] = stud1.name
                break
    if not flag:
        print("ааааааааааааааааааааааааааааааааааааааа")
            
    return class_list

def very_sit(class_list):
    pass

def write_result(class_list):
    with open('result.csv', 'w', encoding="utf8") as f:
        f.write('Место' + ',' + 'Имя' + '\n')
        for k, v in class_list.items():
            f.write(str(k) + ',' + str(v) + '\n')
        f.close()

if '__main__' == __name__:
    student_list = create_child()
    problem_with_child(student_list)
    sort_children_can_sit(student_list)
    clas = sit(student_list)
    write_result(clas)
    

    
            
            
